## Image uploader

###### By Tit Kovalenko

To install app
```
npm i
```

To launch project
```
npm start
```

Any credentials can be used for login.

Images are stored as base64 strings in local storage, so in case of large images or big amount of them expected error can be thrown.

To unstab api requests it is needed to modify methods in modules under ```./src/api``` with plain api calls instead of mocks. This methods should return JSON result promises, all the validation and error handling is in corresponding modules under ```./src/service``` which are used all over the project.