import {createStore} from 'redux';
import {combineReducers} from 'redux';
import ActionTypes from './action-types'

const store = createStore(
    combineReducers({
        tooltipedImages(state = [], { type, payload }) {
            if (type === ActionTypes.TOOPLIPED_IMAGES.ADD_ONE) {
                return [ ...state, payload ];
            }

            if (type === ActionTypes.TOOPLIPED_IMAGES.DELETE_ONE) {
                const index = getItemIndex(payload, state);

                if (index === -1) {
                    return state;
                }

                const newState = [ ...state ];
                newState.splice(index, 1);

                return newState;
            }

            if (type === ActionTypes.TOOPLIPED_IMAGES.UPDATE_ONE) {
                const index = getItemIndex(payload, state);

                if (index === -1) {
                    return state;
                }

                const newState = [ ...state ];
                newState[ index ] = payload;

                return newState;
            }

            if (type === ActionTypes.TOOPLIPED_IMAGES.REFRESH_LIST) {
                return [ ...payload ];
            }

            return state;

            function getItemIndex(item, list) {
                return list.findIndex(itemInList => item.id === itemInList.id);
            }
        },
        session(state = null, { type, payload }) {
            if (type === ActionTypes.SESSION.UPDATE) {
                return {
                    ...payload
                } || null;
            }

            return state;
        },
        errors(state = [], { type, payload }) {
            if (type === ActionTypes.ERRORS.ADD) {
                return [...state, payload];
            }

            if (type === ActionTypes.ERRORS.REMOVE_OLDEST) {
                const newState = [...state];
                newState.splice(0, 1);
                return newState;
            }

            return state;
        }
    }),
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

export default store;
