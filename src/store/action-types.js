const ActionTypes = {
    "ERRORS": {
        "ADD": "ERRORS.ADD",
        "REMOVE_OLDEST": "ERRORS.REMOVE_OLDEST"
    },
    "SESSION": {
        "UPDATE": "SESSION.UPDATE"
    },
    "TOOPLIPED_IMAGES": {
        "ADD_ONE": "TOOPLIPED_IMAGES.ADD_ONE",
        "DELETE_ONE": "TOOPLIPED_IMAGES.DELETE_ONE",
        "REFRESH_LIST": "TOOPLIPED_IMAGES.REFRESH_LIST",
        "UPDATE_ONE": "TOOPLIPED_IMAGES.UPDATE_ONE"
    }
};

export default ActionTypes;