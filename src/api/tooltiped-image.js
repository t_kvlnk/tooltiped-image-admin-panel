import Mock from './mock';
import Toolbox from '../services/toolbox';

const TooltipedImageApi = {
    async delete(id) {
        return new Promise((resolve, reject) => {
            setTimeout(() => {

                const appStorage = Mock.getAppStorage();

                const tooltipedImages = appStorage.tooltipedImages.filter(image => !image.deleted);

                for (let tooltipedImage of tooltipedImages) {
                    if (tooltipedImage.id !== id) {
                        continue;
                    }

                    tooltipedImage.deleted = true;

                    delete tooltipedImage.imageUrl;
                    delete tooltipedImage.tooltip;

                    resolve({
                        payload: null
                    });

                    return
                }

                reject({
                    error: {
                        message: `Delete image error: Image with id:${id} was not found`
                    }
                })
            }, Mock.DEFAULT_DELAY_MS);
        })
    },

    async getById(id) {
        return new Promise((resolve, reject) => {
            const tooltipedImage = Mock.getStoredTooltipedImageById(id);

            setTimeout(() => {
                if (tooltipedImage) {
                    resolve({
                        payload: Mock.deepClone(tooltipedImage)
                    });
                } else {
                    reject({
                        error: {
                            message: `Get image error: Image with id:${id} was not found`
                        }
                    })
                }

            }, Mock.DEFAULT_DELAY_MS)
        })
    },

    async getAll() {
        return new Promise(resolve => {
            const appStorage = Mock.getAppStorage();
            const tooltipedImages = appStorage.tooltipedImages.filter(image => !image.deleted);

            setTimeout(
                resolve.bind(null, {
                    payload: Mock.deepClone(tooltipedImages)
                }),
                Mock.DEFAULT_DELAY_MS
            );
        })
    },

    async update({ id, tooltip, imageBlob, pointerEnabled }) {
        return new Promise(
            (resolve, reject) =>
                setTimeout(async () => {
                    const appStorage = Mock.getAppStorage();

                    const tooltipedImage = Mock.getStoredTooltipedImageById(id, appStorage);

                    if (!tooltipedImage) {
                        reject({
                            error: {
                                message: `Update image tooltip: Image with id:${id} was not found`
                            }
                        });

                        return;
                    }

                    if (imageBlob) {
                        tooltipedImage.imageUrl = await Toolbox.blobToBase64(imageBlob);
                    }

                    tooltipedImage.pointerEnabled = !!pointerEnabled;

                    tooltipedImage.tooltip = tooltip
                                             ? tooltip.toString()
                                             : '';

                    try {
                        Mock.updateAppStorage(appStorage);

                        resolve({
                            payload: Mock.deepClone(tooltipedImage)
                        })
                    } catch (error) {
                        reject({ error })
                    }
                }, Mock.DEFAULT_DELAY_MS)
        );
    },

    async create({ imageBlob, tooltip, pointerEnabled }) {
        return new Promise((resolve, reject) => {
            setTimeout(async () => {
                const imageUrl = await Toolbox.blobToBase64(imageBlob);
                const appStorage = Mock.getAppStorage();

                const id = getNewId(appStorage.tooltipedImages);

                const newTooltipedImage = {
                    id,
                    imageUrl,
                    tooltip,
                    pointerEnabled: !!pointerEnabled
                };

                appStorage.tooltipedImages.push(newTooltipedImage);

                try {
                    Mock.updateAppStorage(appStorage);

                    resolve({
                        payload: Mock.deepClone(newTooltipedImage)
                    })
                } catch (error) {
                    reject({ error })
                }
            }, Mock.DEFAULT_DELAY_MS)
        });

        function getNewId(list) {
            if (!list.length) {
                return 1;
            }

            const ids = list.map(item => item.id);

            return Math.max(...ids) + 1;
        }
    }
};

export default TooltipedImageApi;

