const Mock = {
    DEFAULT_DELAY_MS: 500,
    getAppStorage,
    getStoredTooltipedImageById,
    updateAppStorage,
    deepClone
};

export default Mock;

function getAppStorage() {
    let storage;

    try {
        storage = JSON.parse(localStorage.appStorage || '{}');
    } catch (e) {
        console.error(e);
    }

    if (typeof storage !== 'object') {
        storage = {}
    }

    storage.tooltipedImages = Array.isArray(storage.tooltipedImages)
                              ? storage.tooltipedImages
                              : [];

    storage.sessions = (storage.sessions && typeof storage.sessions === 'object')
                       ? storage.sessions
                       : {};

    return storage;
}

function getStoredTooltipedImageById(id, storage = getAppStorage()) {
    return storage.tooltipedImages.find(
        image =>
            !image.deleted
            && image.id === id
    );
}

function updateAppStorage(newStorage) {
    const stringToStore = JSON.stringify(newStorage);

    try {
        localStorage.appStorage = stringToStore;
    } catch (e) {
        throw new Error('The image you are trying to upload will overflow local storage')
    }
}

function deepClone(obj) {
    let result = null;

    try {
        result = JSON.parse(JSON.stringify(obj));
    } catch (e) {
        console.error(e)
    }

    return result;
}