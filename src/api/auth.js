import Mock from './mock';

const AuthApi = {
    async login({login, password}) {

        return new Promise(resolve => {
            setTimeout(() => {
                const accessToken = Math.floor(Math.random() * 100000).toString();

                const appStorage = Mock.getAppStorage();

                appStorage.sessions[accessToken] = {
                    username: login
                };

                Mock.updateAppStorage(appStorage);

                resolve({
                    payload: {
                        accessToken,
                        username: login,
                        role: 'Admin'
                    }
                })
            }, Mock.DEFAULT_DELAY_MS);
        })
    },

    async logout(accessToken) {
        return new Promise(resolve => {
            setTimeout(() => {
                const appStorage = Mock.getAppStorage();

                delete appStorage.sessions[accessToken];

                Mock.updateAppStorage(appStorage);

                resolve({
                    payload: true
                });
            }, 250);
        })
    },

    async relogin(accessToken) {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                const appStorage = Mock.getAppStorage();
                const activeSession = appStorage.sessions[accessToken];

                if (!activeSession) {
                    reject({
                        error: 'Session expired'
                    });

                    return;
                }

                resolve({
                    payload: {
                        accessToken,
                        username: activeSession.username,
                        role: 'Admin'
                    }
                })
            }, Mock.DEFAULT_DELAY_MS);
        })
    }
};

export default AuthApi;