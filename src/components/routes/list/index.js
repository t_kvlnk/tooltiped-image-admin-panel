import './styles.scss';

import React from 'react';
import {connect} from 'react-redux';
import TooltipedImageService from '../../../services/tooltiped-image';
import {withRouter} from 'react-router-dom';
import {compose} from 'redux';
import Loading from '../../loading';
import TooltipedImageCard from '../../tooltiped-image/card';

const CSS_NSP = 'image-uploader-list-route';

const ListRoute = compose(
    connect(
        state => ({
            tooltipedImages: state.tooltipedImages
        })
    ),
    withRouter
)(
    class ListRoute extends React.Component {
        constructor(props) {
            super(props);

            this.state = {
                loading: true
            }
        }

        async componentWillMount() {
            await TooltipedImageService.getAll();
            this.setState({
                ...this.state,
                loading: false
            })
        }

        render() {
            const { state, props } = this;

            return state.loading
                   ? <Loading/>
                   : (
                       <div className={CSS_NSP}>
                           <ul className={`${CSS_NSP}__list`}>
                               {
                                   props.tooltipedImages.map(tooltipedImage => (
                                       <TooltipedImageCard key={tooltipedImage.id}
                                                           tooltipedImage={tooltipedImage}/>
                                   ))
                               }
                           </ul>
                           <button onClick={() => props.history.push('/upload')}>
                               Add
                           </button>
                       </div>
                   );
        }
    }
);

export default ListRoute;