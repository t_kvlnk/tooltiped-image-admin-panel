import './styles.scss';
import React from 'react';
import TooltipedImageService from '../../../services/tooltiped-image';
import {Link, withRouter} from 'react-router-dom';
import TooltipedImagePreview from '../../tooltiped-image/preview';
import Loading from '../../loading';

const CSS_NSP = 'image-uploader-preview-route';

const PreviewRoute = withRouter(
    class PreviewRoute extends React.Component {
        constructor(props) {
            super(props);

            this.state = {
                loading: true,
                tooltipedImage: null
            };
        }

        async componentDidMount() {
            const id = parseInt(this.props.match.params.id, 10);

            if (!id) {
                return
            }

            try {
                const tooltipedImage = await TooltipedImageService.getById(id);

                this.setState({
                    tooltipedImage,
                    loading: false
                })
            } catch (e) {}
        }

        render() {
            return this.state.loading
                   ? <Loading/>
                   : (
                       <div className={CSS_NSP}>
                           <TooltipedImagePreview tooltipedImage={this.state.tooltipedImage}/>
                           <div className={`${CSS_NSP}__back-link`}>
                               <Link to='/'>Back to list</Link>
                           </div>
                       </div>
                   )
        }
    }
);

export default PreviewRoute;

