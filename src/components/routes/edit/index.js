import './styles.scss';

import React from 'react';
import {Link, withRouter} from 'react-router-dom';
import TooltipedImageService from '../../../services/tooltiped-image';
import TooltipedImageEditor from '../../tooltiped-image/editor';
import Loading from '../../loading';

const CSS_NSP = 'image-uploader-edit-route';

const EditRoute = withRouter(
    class EditRoute extends React.Component {
        constructor(props) {
            super(props);

            this.tooltipedImage = null;

            this.state = {
                loading: true
            }
        }

        async componentDidMount() {
            const id = parseInt(this.props.match.params.id, 10);

            if (id) {
                try {
                    this.tooltipedImage = await TooltipedImageService.getById(id);
                } catch (e) {}
            }

            this.setState({
                loading: false
            });
        }

        render() {
            return this.state.loading
                   ? <Loading/>
                   : (
                       <div className={CSS_NSP}>
                           <TooltipedImageEditor tooltipedImage={this.tooltipedImage}/>
                           <div className={`${CSS_NSP}__back-link`}>
                               <Link to='/'>Back to list</Link>
                           </div>
                       </div>
                   );
        }
    }
);

export default EditRoute;