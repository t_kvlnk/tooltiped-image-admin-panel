import './styles.scss';

import React from 'react';
import TooltipedImageService from '../../../services/tooltiped-image';
import {withRouter} from 'react-router-dom';
import {compose} from 'redux';
import TooltipedImagePreview from '../preview';

const CSS_NSP = 'image-uploader-tooltiped-image-card';

const TooltipedImageCard = compose(
    withRouter
)(
    class TooltipedImageCard extends React.Component {
        render() {
            const { props } = this;
            const { tooltipedImage } = props;

            return (
                <div className={CSS_NSP}>
                    <TooltipedImagePreview className={`${CSS_NSP}__image`}
                                           hidePointer={true}
                                           tooltipedImage={tooltipedImage}/>

                    <div className={`${CSS_NSP}__tooltip`}
                         title={tooltipedImage.tooltip}>
                        {tooltipedImage.tooltip || <br/>}
                    </div>

                    <div>
                        <b>
                            Pointer {tooltipedImage.pointerEnabled ? 'enabled' : 'disabled'}
                        </b>
                    </div>

                    <div className={`${CSS_NSP}__buttons`}>
                        <button onClick={() => props.history.push(`/preview/${tooltipedImage.id}`)}>
                            Preview
                        </button>

                        <button onClick={() => props.history.push(`/edit/${tooltipedImage.id}`)}>
                            Edit
                        </button>

                        <button onClick={
                            async ({ target }) => {
                                target.disabled = true;
                                await TooltipedImageService.delete(tooltipedImage);
                                target.disabled = false;
                            }
                        }>
                            Delete
                        </button>
                    </div>
                </div>
            );
        }
    }
);

export default TooltipedImageCard;