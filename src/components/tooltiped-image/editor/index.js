import './styles.scss';

import React from 'react';
import {withRouter} from 'react-router-dom';
import Toolbox from '../../../services/toolbox';
import TooltipedImageService from '../../../services/tooltiped-image';
import TooltipedImagePreview from '../preview';

const CSS_NSP = 'image-uploader-tooltiped-image-editor';

const TooltipedImageEditor = withRouter(
    class TooltipedImageEditor extends React.Component {
        constructor(props) {
            super(props);

            this.newImageBlob = null;
            this.newTooltipVal = this.props.tooltipedImage
                                 ? this.props.tooltipedImage.tooltip
                                 : '';

            this.newPointerEnabled = this.props.tooltipedImage
                                     ? this.props.tooltipedImage.pointerEnabled
                                     : '';

            this.state = {
                imageUrl: this.props.tooltipedImage
                          ? this.props.tooltipedImage.imageUrl
                          : '',
                pending: false
            }
        }

        submit = async (evt) => {
            try {
                evt.preventDefault();

                this.setState({
                    pending: true
                });

                if (this.props.tooltipedImage) {
                    const updatedTooltipedImage = { ...this.props.tooltipedImage };

                    if (this.newImageBlob) {
                        updatedTooltipedImage.imageBlob = this.newImageBlob;
                    }

                    if (this.newTooltipVal
                        && this.newTooltipVal !== updatedTooltipedImage.tooltip) {

                        updatedTooltipedImage.tooltip = this.newTooltipVal;
                    }


                    updatedTooltipedImage.pointerEnabled = this.newPointerEnabled;

                    await TooltipedImageService.update(updatedTooltipedImage);
                } else {
                    await TooltipedImageService.create({
                        imageBlob: this.newImageBlob,
                        tooltip: this.newTooltipVal || ''
                    });
                }

                this.props.history.push('/');
            } catch (e) {
                this.setState({
                    pending: false
                });
            }
        };

        fileChanged = async ({ target }) => {
            this.newImageBlob = target.files.item(0);

            if (!this.newImageBlob) {
                this.setState({
                    imageUrl: ''
                });

                return;
            }

            const imageUrl = await Toolbox.blobToBase64(this.newImageBlob);

            this.setState({ imageUrl });
        };

        render() {
            return (
                <div className={CSS_NSP}>

                    <div className={`${CSS_NSP}__file-picker`}>
                        <input type="file"
                               onChange={this.fileChanged}/>
                        <div className={`${CSS_NSP}__file-picker__area-disclaimer`}>
                            <h4>
                                Drop image here
                            </h4>

                            <h5>
                                Or click here<br/>to select image
                            </h5>
                        </div>
                    </div>

                    {
                        this.state.imageUrl
                        &&
                        <form className={`${CSS_NSP}__preview`}
                              onSubmit={this.submit}>

                            <TooltipedImagePreview className={`${CSS_NSP}__preview__image`}
                                                   hidePointer={true}
                                                   tooltipedImage={{ imageUrl: this.state.imageUrl }}/>

                            <label className={`${CSS_NSP}__preview__tooltip`}>
                                <div>
                                    Tooltip:
                                </div>
                                <textarea disabled={this.state.pending}
                                          rows={3}
                                          onChange={({ target }) => {this.newTooltipVal = target.value}}
                                          ref={input => {
                                              if (!input) {
                                                  return;
                                              }

                                              if (typeof this.newTooltipVal === 'string') {
                                                  input.value = this.newTooltipVal;
                                              }
                                          }}/>
                            </label>

                            <label className={`${CSS_NSP}__preview__pointer-enabled`}>
                                <input type="checkbox"
                                       disabled={this.state.pending}
                                       onChange={({ target }) => this.newPointerEnabled = target.checked}
                                       ref={input => {
                                           if (!input) {
                                               return;
                                           }

                                           input.checked = this.newPointerEnabled;
                                       }}/>

                                <span>
                                    Pointer enabled
                                </span>
                            </label>

                            <button type='submit'
                                    disabled={this.state.pending}>
                                Submit
                            </button>
                        </form>
                    }
                </div>
            );
        }
    }
);

export default TooltipedImageEditor;