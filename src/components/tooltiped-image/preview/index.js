import './styles.scss';
import React from 'react';

const CSS_NSP = 'image-uploader-tooltiped-image-preview';

const TooltipedImagePreview = (props) => (
    <div className={CSS_NSP}>
        <div className={`${CSS_NSP}__image`}
             style={{
                 backgroundImage: `url(${props.tooltipedImage.imageUrl})`
             }}/>
        {
            !props.hidePointer
            && props.tooltipedImage.pointerEnabled
            &&
            <div className={`${CSS_NSP}__tooltip-pointer`}>
                <div className={`${CSS_NSP}__tooltip-pointer__pointer`}>
                    ?
                </div>
                <div className={`${CSS_NSP}__tooltip-pointer__tooltip`}>
                    {props.tooltipedImage.tooltip}
                </div>
            </div>
        }
    </div>
);

export default TooltipedImagePreview;