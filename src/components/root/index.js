import './styles.scss';

import React from 'react'
import {Route, BrowserRouter as Router, Redirect, Switch} from 'react-router-dom';
import LoginForm from '../login-form';
import ListRoute from '../routes/list';
import PreviewRoute from '../routes/preview';
import EditRoute from '../routes/edit';
import {connect} from 'react-redux';
import AuthService from '../../services/auth';
import Loading from '../loading';
import Header from '../header';
import ErrorsContainer from '../errors-container';

const CSS_NSP = 'image-uploader-root';

export default class Root extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            pending: true
        }
    }

    async componentWillMount() {
        try {
            if (localStorage.accessToken) {
                await AuthService.relogin(localStorage.accessToken);
            }
        } catch (e) {}

        this.setState({
            pending: false
        });
    }

    render() {
        return (
            <div className={CSS_NSP}>
                {
                    this.state.pending
                    ? <Loading/>
                    : (
                        <Router>
                            <Switch>
                                <Route path='/'
                                       exact
                                       component={authRequired(ListRoute)}/>
                                <Route path='/upload'
                                       component={authRequired(EditRoute)}/>
                                <Route path='/edit/:id([0-9]+)'
                                       component={authRequired(EditRoute)}/>
                                <Route path='/preview/:id([0-9]+)'
                                       component={authRequired(PreviewRoute)}/>
                                <Route path="*" render={() => (<Redirect to='/'/>)}/>
                            </Switch>
                        </Router>
                    )
                }
                <ErrorsContainer/>
            </div>
        )
    }
}

const AuthRoute = connect(
    state => ({
        accessToken: state.session
                     && state.session.accessToken
    })
)(
    class AuthRoute extends React.Component {
        render() {
            const { component: Component, ...rest } = this.props;

            return (this.props.accessToken)
                   ? (
                       <React.Fragment>
                           <Header/>
                           <div className={`${CSS_NSP}__main`} >
                               <Component {...rest} />
                           </div>
                       </React.Fragment>
                   )
                   : <LoginForm/>
        }
    }
);

const authRequired = component => () => <AuthRoute component={component}/>;