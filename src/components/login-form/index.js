import './styles.scss';

import React from 'react';
import AuthService from '../../services/auth';
import {withRouter} from 'react-router-dom';

const CSS_NSP = 'image-uploader-login-form';

const LoginForm = withRouter(
    class LoginForm extends React.Component {
        constructor(props) {
            super(props);

            this.state = {
                pending: false
            }
        }

        submit = async (evt) => {
            evt.preventDefault();

            this.setState({
                pending: true
            });

            try {
                await AuthService.login({
                    login: this.loginInput.value,
                    password: this.passwordInput.value
                });
            } catch (e) {
                this.setState({
                    pending: false
                });
            }
        };

        render() {
            return (
                <form onSubmit={this.submit}
                      className={CSS_NSP}>
                    <label>
                        <div>
                            Username
                        </div>
                        <input type="text"
                               disabled={this.state.pending}
                               ref={input => this.loginInput = input}/>
                    </label>

                    <br/>
                    <br/>

                    <label>
                        <div>
                            Password
                        </div>
                        <input type="password"
                               disabled={this.state.pending}
                               ref={input => this.passwordInput = input}/>
                    </label>

                    <br/>
                    <br/>

                    <button type="submit"
                            disabled={this.state.pending}>
                        Login
                    </button>
                </form>
            )
        }
    }
);

export default LoginForm