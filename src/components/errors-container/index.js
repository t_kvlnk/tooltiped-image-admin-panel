import './styles.scss'

import React from 'react';
import {connect} from 'react-redux';

const CSS_NSP = 'image-uploader-errors-container';

const ErrorsContainer = connect(
    state => ({
        errors: state.errors
    })
)(
    class extends React.Component {
        render() {
            return (
                <div className={CSS_NSP}>
                    {
                        this.props.errors.map((e, i) =>
                            (
                                <div className={`${CSS_NSP}__item`}
                                     key={i}>

                                    {
                                        e.error.message
                                        || e.message
                                        || e.error
                                    }
                                </div>
                            )
                        )
                    }
                </div>
            )
        }
    }
);

export default ErrorsContainer;