import './styles.scss';
import React from 'react';

const CSS_NSP = 'image-uploader-loading';

const Loading = () => (
    <div className={CSS_NSP}>
        Loading...
    </div>
);

export default Loading;