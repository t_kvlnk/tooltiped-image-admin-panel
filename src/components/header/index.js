import './styles.scss';

import {connect} from 'react-redux';
import React from 'react';
import {compose} from 'redux';
import {withRouter} from 'react-router-dom';
import AuthService from '../../services/auth';

const CSS_NSP = 'image-uploader-header';

const Header = compose(
    connect(
        state => ({
            username: state.session
                      && state.session.username,
            accessToken: state.session
                         && state.session.accessToken
        })
    ),
    withRouter
)(
    class Header extends React.Component {
        constructor(props) {
            super(props);

            this.state = {
                pending: false
            }
        }

        logout = async () => {
            this.setState({
                pending: true
            });

            try {
                await AuthService.logout(this.props.accessToken);
                this.props.history.push('/')
            } catch (e) {
                this.setState({
                    pending: false
                });
            }
        };

        render() {
            return (
                <div className={CSS_NSP}>
                    <div>
                        {`Hello, ${this.props.username}!`}
                    </div>
                    <button onClick={this.logout}
                            disabled={this.state.pending}>
                        Logout
                    </button>
                </div>
            )
        }
    }
);

export default Header;