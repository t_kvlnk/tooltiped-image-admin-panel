import AuthApi from '../api/auth';
import ErrorService from './error';
import store from '../store';
import ActionTypes from '../store/action-types';

const AuthService = {
    async login(credentials = {}) {
        const err = validate(...arguments);

        if (err) {
            return ErrorService.notifyError(err);
        }

        try {
            const {payload} = await AuthApi.login(credentials);

            localStorage.accessToken = payload.accessToken;

            store.dispatch({
                type: ActionTypes.SESSION.UPDATE,
                payload
            });

            return payload;
        } catch (e) {
            return ErrorService.notifyError(e)
        }

        function validate({ login, password }) {
            if (!login
                || typeof login !== 'string') {

                return new Error('Login is invalid or absent');
            }

            if (!password
                || typeof password !== 'string') {

                return new Error('Password is invalid or absent');
            }
        }
    },

    async logout(accessToken) {
        const err = validate(...arguments);

        if (err) {
            return ErrorService.notifyError(err);
        }

        try {
            await AuthApi.logout(accessToken);

            store.dispatch({
                type: ActionTypes.SESSION.UPDATE,
                payload: null
            });
        } catch (e) {
            return ErrorService.notifyError(e);
        }

        function validate(accessToken) {
            if (!accessToken
                || typeof accessToken !== 'string') {

                return new Error('Access token is invalid or absent');
            }
        }
    },

    async relogin(accessToken) {
        const err = validate(...arguments);

        if (err) {
            return ErrorService.notifyError(err);
        }

        try {
            const {payload} = await AuthApi.relogin(accessToken);

            store.dispatch({
                type: ActionTypes.SESSION.UPDATE,
                payload
            });
        } catch (e) {
            return ErrorService.notifyError(e);
        }

        function validate(accessToken) {
            if (!accessToken
                || typeof accessToken !== 'string') {

                return new Error('Access token is invalid or absent');
            }
        }
    }
};

export default AuthService;