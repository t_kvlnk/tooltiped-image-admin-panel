import store from '../store';
import ActionTypes from '../store/action-types';

const ErrorService = {
    notifyError(err = new Error('Unknown error')) {
        const rejectedPromise = Promise.reject(err);

        rejectedPromise.catch(() => {
            store.dispatch({
                type: ActionTypes.ERRORS.ADD,
                payload: err
            });

            setTimeout(() => {
                store.dispatch({
                    type: ActionTypes.ERRORS.REMOVE_OLDEST
                })
            }, 3000)
        });

        return rejectedPromise
    }
};

export default ErrorService;