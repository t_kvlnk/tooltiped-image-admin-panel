import TooltipedImageApi from '../api/tooltiped-image';
import ErrorService from './error';
import store from '../store';
import ActionTypes from '../store/action-types';

const TooltipedImageService = {
    async delete(tooltipedImage) {
        const err = validate(...arguments);

        if (err) {
            return ErrorService.notifyError(err);
        }

        try {
            const { payload } = await TooltipedImageApi.delete(tooltipedImage.id);

            store.dispatch({
                type: ActionTypes.TOOPLIPED_IMAGES.DELETE_ONE,
                payload: tooltipedImage
            });

            return payload;
        } catch (e) {
            return ErrorService.notifyError(e);
        }

        function validate(tooltipedImage) {
            if (!tooltipedImage) {
                return new Error(`Tooltiped image is required`);
            }

            if (!isValidId(tooltipedImage.id)) {

                return new Error(`Tooltiped image must a positive number as id property`);
            }
        }
    },

    async getById(id) {
        const err = validate(...arguments);

        if (err) {
            return ErrorService.notifyError(err)
        }

        try {
            const { payload } = await TooltipedImageApi.getById(id);

            store.dispatch({
                type: ActionTypes.TOOPLIPED_IMAGES.UPDATE_ONE,
                payload
            });

            return payload;
        } catch (e) {
            return ErrorService.notifyError(err);
        }

        function validate(id) {
            if (!isValidId(id)) {

                return new Error('Tooltiped image id must be a positive number')
            }
        }
    },

    async getAll() {
        try {
            const { payload } = await TooltipedImageApi.getAll();

            store.dispatch({
                type: ActionTypes.TOOPLIPED_IMAGES.REFRESH_LIST,
                payload
            });

            return payload;
        } catch (e) {
            return await ErrorService.notifyError(e);
        }
    },

    async update(tooltipedImage) {
        const err = validate(...arguments);

        if (err) {
            return await ErrorService.notifyError(err);
        }

        try {
            const { payload } = await TooltipedImageApi.update(tooltipedImage);

            store.dispatch({
                type: ActionTypes.TOOPLIPED_IMAGES.UPDATE_ONE,
                payload
            });

            return payload;
        } catch (e) {
            return await ErrorService.notifyError(e)
        }

        function validate({ id, tooltip, imageBlob }) {
            if (!isValidId(id)) {
                return new Error(`Tooltiped image must a positive number as id property`);
            }

            if (tooltip
                && typeof tooltip !== 'string') {
                return new Error('New tooltip must be a string type')
            }

            if (imageBlob
                && isValidImabeBlob(imageBlob)) {
                return new Error('Uploaded is not an image')
            }
        }
    },

    async create(tooltipedImage) {
        const err = validate(...arguments);

        if (err) {
            return await ErrorService.notifyError(err);
        }

        try {
            const { payload } = await TooltipedImageApi.create(tooltipedImage);

            store.dispatch({
                type: ActionTypes.TOOPLIPED_IMAGES.ADD_ONE,
                payload
            });

            return payload;
        } catch (e) {
            return await ErrorService.notifyError(e)
        }

        function validate({ imageBlob }) {
            if (!imageBlob) {
                return new Error('Image is required');
            }

            if (isValidImabeBlob(imageBlob)) {
                return new Error('Files except images are prohibited');
            }
        }
    }
};

export default TooltipedImageService;

function isValidId(id) {
    return id
           && typeof id === 'number'
           && id > 0
}

function isValidImabeBlob(blob) {
    return blob instanceof Blob
           && ![ 'image/jpeg', 'image/png' ].includes(blob.type)
}