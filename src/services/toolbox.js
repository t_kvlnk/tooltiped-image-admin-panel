const Toolbox = {
    blobToBase64
};

export default Toolbox;

async function blobToBase64(blob) {
    return new Promise((resolve, reject) => {
        const reader = new FileReader();

        reader.readAsDataURL(blob);

        reader.addEventListener('load', () => {
            resolve(reader.result);
        });

        reader.addEventListener('error', err => {
            reject(err)
        })
    });
}